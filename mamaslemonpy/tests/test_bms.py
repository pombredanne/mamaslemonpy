import random
import mamaslemonpy
from mamaslemonpy.suffix import SuffixArray

_CHARS = "$ACGT."
_DNA = (1,2,3,4)*95 + (5,5,5,5)*5
_OPT = (False,)*90 + (True,)*10

def random_pattern(length):
    p = [random.choice(_DNA) for i in range(length)]
    o = [random.choice(_OPT) for i in range(length)]
    return p, o

def run_ms(sa, p, o=None):
    bms = list(sa.backward_matching_statistics(p))
    bms2 = list(sa.backward_matching_statistics_with_optional_characters(p,o))
    M = max(len(bms),len(bms2))
    oldms1 = 0;  oldms2 = 0
    for t in range(M):
        s1 = bms[t];  s2 = bms2[t]
        j1 = s1[0];  j2 = s2[0]
        c1 = _CHARS[p[j1]] + ("?" if o[j1] else " ")
        c2 = _CHARS[p[j2]] + ("?" if o[j2] else " ")
        ms1 = s1[2];  ms2 = s2[2]
        # we generally have ms1 <= ms2, if they are equal, test
        msg = "  {}/{}  {} / {}".format(c1,c2,s1,s2)
        if ms1 >= ms2:
            assert s1 == s2, msg
        else:
            pass
            #print(msg)
        assert ms1 >= oldms1 - 1, msg
        assert ms2 >= oldms2 - 1, msg
        oldms1 = ms1;  oldms2 = ms2


def main(indexname, num=1000, length=200):
    sa = SuffixArray.from_indexname(indexname)
    sa.bwt_from_indexname(indexname)
    sa.occ_from_indexname(indexname)
    for i in range(num):
        print("PATTERN",i)
        pat, opt = random_pattern(length)
        run_ms(sa, pat, opt)
    pass

if __name__ == "__main__":
    main("./gludna")

