# mamaslemonpy
# (c) 2011--2012 Sven Rahmann
"""provde basic suffix array construction and simple matching algorithms.

To be compatible with geniegui, the __init__ module provides
get_argument_parser() and main() functions.
"""

__version__ = "0.5"
__author__ = "Sven Rahmann"

from .core import main, get_argument_parser

